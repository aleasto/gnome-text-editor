gnome-text-editor (47~rc-1) UNRELEASED; urgency=medium

  * New upstream release

 -- Alessandro Astone <alessandro.astone@canonical.com>  Wed, 04 Sep 2024 17:26:13 +0200

gnome-text-editor (47~beta-1) experimental; urgency=medium

  * New upstream release
  * Bump minimum glib, GTK4, & libadwaita
  * Bump Standards Version to 4.7.0

 -- Jeremy Bícha <jbicha@ubuntu.com>  Fri, 09 Aug 2024 14:57:36 -0400

gnome-text-editor (46.3-1) unstable; urgency=medium

  * New upstream release

 -- Jeremy Bícha <jbicha@ubuntu.com>  Tue, 14 May 2024 10:22:55 +0200

gnome-text-editor (46.1-1) unstable; urgency=medium

  * New upstream release

 -- Jeremy Bícha <jbicha@ubuntu.com>  Thu, 11 Apr 2024 09:00:46 -0400

gnome-text-editor (46.0-1) unstable; urgency=medium

  * Team upload
  * New upstream release
    - 46.rc:
      + Disable editing while document is loading
      + Discard still-loading documents when saving session
      + Fix some tab integration issues with libadwaita
    - 46.0:
      + Various performance workarounds have been added to the
        overview map to improve rendering performance and quality
        with fractional scaling.
  * debian/control: Bump minimum required libgtk-4-dev to 4.12
  * debian/control: Update Homepage to the new page on apps.gnome.org

 -- Amin Bandali <bandali@ubuntu.com>  Tue, 19 Mar 2024 09:36:23 -0400

gnome-text-editor (46~beta-1) unstable; urgency=medium

  * Team upload
  * New upstream release
    - 46.alpha:
      + Make various UI elements conform better to GNOME HIG
      + Fix etag usage when comparing changes from GFileMonitor
      + Cancel document loading when tab or window closes.
        This fixes an issue where you could have 100% CPU doing
        Pango line breaking on extremely large files even after
        closing a window or tab.
      + Improve margin for RTL languages
      + Updated appdata/metainfo
      + Fix memory leak in usage of GtkNativeDialog
      + Work around some broken user configurations for XDG
        special directories
    - 46.beta:
      + Use AdwAboutDialog
      + Fix keyboard access to light/dark/follow theme selectors
      + Use GtkFontDialog instead of deprecated GtkFontChooserDialog
      + Check for files on disk before restoring drafts to avoid
        displaying deleted files.
      + Performance improvements to the minimap on GTKs new renderer
      + Avoid draft auto-save file the document is still loading
      + The language selection dialog is now modal
      + A new-window action was added to .desktop file for GNOME Shell
      + Improve support for closing a page while it is still loading
  * debian/control: Bump minimum required versions in Build-Depends
    following upstream
  * debian/copyright: Drop a vestigial Files paragraph

 -- Amin Bandali <bandali@ubuntu.com>  Tue, 27 Feb 2024 08:10:44 -0500

gnome-text-editor (45.1-1) unstable; urgency=medium

  * New upstream release
  * Stop using debian/control.in and dh_gnome_clean

 -- Jeremy Bícha <jbicha@ubuntu.com>  Mon, 27 Nov 2023 09:50:50 -0500

gnome-text-editor (45.0-1) unstable; urgency=medium

  * New upstream release

 -- Jeremy Bícha <jbicha@ubuntu.com>  Mon, 18 Sep 2023 08:11:30 -0400

gnome-text-editor (45~beta-2) unstable; urgency=medium

  * Release to unstable

 -- Jeremy Bícha <jbicha@ubuntu.com>  Wed, 30 Aug 2023 18:51:14 -0400

gnome-text-editor (45~beta-1) experimental; urgency=medium

  * New upstream release
  * debian/control.in: Bump minimum libadwaita to 1.4~beta

 -- Jeremy Bícha <jbicha@ubuntu.com>  Wed, 09 Aug 2023 14:27:01 -0400

gnome-text-editor (44.0-2) unstable; urgency=medium

  * Update standards version to 4.6.2, no changes needed
  * Release to unstable

 -- Jeremy Bícha <jbicha@ubuntu.com>  Sun, 23 Jul 2023 15:38:13 -0400

gnome-text-editor (44.0-1) experimental; urgency=medium

  * New upstream release

 -- Jeremy Bicha <jbicha@ubuntu.com>  Mon, 27 Mar 2023 10:17:47 -0400

gnome-text-editor (44~rc-1) experimental; urgency=medium

  * New upstream release

 -- Jeremy Bicha <jbicha@ubuntu.com>  Wed, 08 Mar 2023 13:27:33 -0500

gnome-text-editor (44~beta-1) experimental; urgency=medium

  * New upstream release
  * Bump minimum meson

 -- Jeremy Bicha <jbicha@ubuntu.com>  Thu, 16 Feb 2023 07:57:53 -0500

gnome-text-editor (43.2-1) unstable; urgency=medium

  * New upstream release

 -- Jeremy Bicha <jbicha@ubuntu.com>  Fri, 13 Jan 2023 09:31:54 -0500

gnome-text-editor (43.1-2) unstable; urgency=medium

  [ Sebastien Bacher ]
  * debian/tests/version:
    - fix the case arguments order being reversed

  [ Jeremy Bicha ]
  * debian/tests/control: Drop unneeded allow-stderr
  * Drop patch: no longer needed

 -- Jeremy Bicha <jbicha@ubuntu.com>  Tue, 11 Oct 2022 13:57:17 -0400

gnome-text-editor (43.1-1) unstable; urgency=medium

  * New upstream release
  * debian/control.in: Set Rules-Requires-Root: no
  * debian/rules: Set bugreport URL for Debian and Ubuntu
  * Drop a patch applied in the new release

 -- Jeremy Bicha <jbicha@ubuntu.com>  Tue, 04 Oct 2022 17:23:45 -0400

gnome-text-editor (43.0-3) unstable; urgency=medium

  * Drop a patch that broke Debian's autopkgtest

 -- Jeremy Bicha <jbicha@ubuntu.com>  Tue, 04 Oct 2022 16:05:14 -0400

gnome-text-editor (43.0-2) unstable; urgency=medium

  * Cherry-pick 2 patches to fix some issues

 -- Jeremy Bicha <jbicha@ubuntu.com>  Tue, 20 Sep 2022 16:29:41 -0400

gnome-text-editor (43.0-1) unstable; urgency=medium

  [ Jeremy Bicha ]
  * New upstream release
  * Revert "Add patch to not require gtk4 4.7 or glib 2.73 yet"
  * debian/control.in: Bump minimum gtk4 and glib
  * Add debian/links to install short gted alias (LP: #1987556)
  * Add superficial autopkgtest

  [ Debian Janitor ]
  * Set upstream metadata fields
  * Remove unused license definitions for LGPL-2+
  * Update standards version to 4.6.1, no changes needed

 -- Jeremy Bicha <jbicha@ubuntu.com>  Tue, 20 Sep 2022 08:50:08 -0400

gnome-text-editor (43~alpha1-1) unstable; urgency=medium

  * New upstream release
  * Add patch to not require gtk4 4.7 or glib 2.73 yet
  * debian/control.in: Bump minimum gtksourceview5 to 5.5.0
  * debian/control.in: Bump minimum libadwaita to 1.2~alpha
  * debian/control.in: Build-Depend on libeditorconfig-dev

 -- Jeremy Bicha <jbicha@ubuntu.com>  Tue, 09 Aug 2022 10:57:09 -0400

gnome-text-editor (42.2-1) unstable; urgency=medium

  * Team upload
  * New upstream release (LP: #1978700)

 -- Nathan Pratta Teodosio <nathan.teodosio@canonical.com>  Mon, 13 Jun 2022 14:33:42 -0300

gnome-text-editor (42.1-1) unstable; urgency=medium

  * New upstream release (LP: #1970041)
  * Bump minimum gtksourceview5 to 5.4.1
  * Drop patch: applied in new release

 -- Jeremy Bicha <jbicha@ubuntu.com>  Sat, 23 Apr 2022 11:53:16 -0400

gnome-text-editor (42.0-2) unstable; urgency=medium

  * Cherry-pick patch to fix appstream validation test

 -- Jeremy Bicha <jbicha@ubuntu.com>  Sat, 19 Mar 2022 07:40:33 -0400

gnome-text-editor (42.0-1) unstable; urgency=medium

  * New upstream release

 -- Jeremy Bicha <jbicha@ubuntu.com>  Fri, 18 Mar 2022 19:56:14 -0400

gnome-text-editor (42~rc1-1) unstable; urgency=medium

  * New upstream release
  * debian/control.in: Bump Standards-Version to 4.6.0

 -- Jeremy Bicha <jeremy.bicha@canonical.com>  Tue, 08 Mar 2022 08:26:27 -0500

gnome-text-editor (42~beta1-1) unstable; urgency=medium

  * New upstream release
  * Drop patch: applied in new release
  * debian/control.in: Bump minimum gtk4 to 4.6

 -- Jeremy Bicha <jeremy.bicha@canonical.com>  Tue, 22 Feb 2022 18:53:06 -0500

gnome-text-editor (42~alpha2-1) experimental; urgency=medium

  * New upstream release
  * debian/control.in: Bump minimum libadwaita, meson, gtk4, & gtksourceview5
  * debian/control.in: Build-Depend on desktop-file-utils
  * debian/control.in: Build-Depend on appstream-util for build tests
  * Drop all patches: applied in new release
  * Cherry-pick patch to fix build

 -- Jeremy Bicha <jbicha@debian.org>  Sun, 16 Jan 2022 20:39:04 -0500

gnome-text-editor (41.1-1) experimental; urgency=medium

  * Initial release (Closes: #996646)

 -- Jeremy Bicha <jbicha@debian.org>  Sat, 27 Nov 2021 16:26:47 -0500
